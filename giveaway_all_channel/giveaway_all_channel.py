import threading
from datetime import datetime
import discum

DISCORD_TOKEN = ""

emoji = '🎉'
keyword = 'React with 🎉 to enter'
blacklist = {'Bot', '機器人', '机器人', 'Don\'t', 'Do not', '請勿', '请勿', '別', 'Test', '測試', '测试'}

bot = discum.Client(token=DISCORD_TOKEN, log={"console": False, "file": False})


@bot.gateway.command
def show_login(resp):
    if resp.event.ready_supplemental:
        user = bot.gateway.session.user
        print("Logged in as {}#{}".format(user['username'], user['discriminator']))


@bot.gateway.command
def resp_giveaway(resp):
    if resp.event.message:
        message = resp.parsed.auto()
        try:
            check_giveaway(message)
        except Exception as e:
            printt(e)


def printt(message):
    full_msg = f'{datetime.now()}: {message}'
    with open("log.txt", "a+") as f:
        f.write(f'{full_msg}\n')
    print(full_msg)


def join_giveaway(message, emoji):
    timer = threading.Timer(3, _do_join_giveaway, args=(message, emoji,))
    timer.start()


def _do_join_giveaway(message, emoji):
    message_id = message['id']
    channel_id = message['channel_id']
    guild_id = message['guild_id']
    bot.addReaction(channel_id, message_id, emoji)
    title = ''
    if 'embeds' in message:
        embed = message['embeds'][0]
        title = embed['title'] if 'title' in embed else embed['description']
    giveaway_url = f'https://discord.com/channels/{guild_id}/{channel_id}/{message_id}'
    printt(f'Join Giveaway - {title} - {giveaway_url}')


def check_giveaway(message):

    if message.get('referenced_message') is not None:
        return
    if message['embeds']:
        embed = message['embeds'][0]
        if 'description' in embed:
            if keyword in embed['description']:
                if 'title' in embed:
                    if any(b.lower() in embed['title'].lower() for b in blacklist):
                        printt(f'Anti-bot detected: ')
                        return
                join_giveaway(message, emoji)
    elif keyword in message['content']:
        join_giveaway(message, emoji)


if __name__ == '__main__':
    bot.gateway.run()
