# WTC自動拉炮腳本
修改自WTC @leosoqqq

##### 不需再指定頻道名稱，會自動對訊息做判斷，遇到抽獎的訊息會自動點擊拉炮。

## Getting started

#### 請先安裝必要套件
```
python -m pip install --user --upgrade git+https://github.com/Merubokkusu/Discord-S.C.U.M.git#egg=discum
```

#### 修改Discord登入訊息
1. 打開giveaway_all_channel.py
2. 修改DISCORD_TOKEN

#### 運行
python giveaway_all_channel.py

#### 結果
成功登入Discord後會看到
```
Logged in as {你的Discord ID}
```

成功拉炮後會看到
```
2022-03-23 21:36:30.785874: Join Giveaway - 拉炮名字 - https://discord.com/channels/9999999999999999999/9999999999999999999/9999999999999999999
```
