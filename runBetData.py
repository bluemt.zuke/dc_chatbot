# -*- coding: utf-8 -*-
import os,json
from http.client import HTTPSConnection
import urllib.parse

op_data_path_discord = os.getcwd()+"/discord_message.json"


class RunBetData(object):
    _robot_connection = None

    def _get_json_data_discord(self):
        '''读取json文件'''
        tmp_json = {}
        with open(op_data_path_discord, 'r', encoding='UTF-8') as f:
            tmp_json = json.load(f)
            f.close()
        return tmp_json

    def get_discord_message_random(self, index):
        data_json = self._get_json_data_discord()
        message_array =  data_json["bot"]
        return message_array[index]["message"]

    def get_discord_link(self):
        data_json = self._get_json_data_discord()
        message_array =  data_json["config"]
        return message_array["discord_link"]

    def get_discord_token(self):
        data_json = self._get_json_data_discord()
        message_array =  data_json["config"]
        return message_array["token"]

    def get_discord_channel_id(self):
        data_json = self._get_json_data_discord()
        message_array =  data_json["config"]
        return message_array["channel_id"]

    def get_discord_time_interval(self):
        data_json = self._get_json_data_discord()
        message_array = data_json["config"]
        return message_array["time_interval"]

    def _get_robot_connection(self):
        if self._robot_connection is None:
            self._robot_connection = HTTPSConnection("api.ownthink.com", 443)
        return self._robot_connection

    def get_robot_reply(self, message):
        conn = self._get_robot_connection()
        path = "/bot?appid=xiaosi&userid=user&spoken={}".format(urllib.parse.quote_plus(message))
        conn.request("GET", path, None)
        resp = conn.getresponse()

        if 199 < resp.status < 300:
            resp_data = json.loads(resp.read())
            if resp_data['message'] == 'success':
                return resp_data['data']['info']['text']
        return None


if __name__ == "__main__":
    instance = RunBetData()
