from datetime import datetime
import traceback
import requests
import config
import sys

DC_NAME = "zukecsie|WTC|AJ"  # DC的個人名稱(不是伺服器名稱)，不用後面的#編號


def printt(message):
    print(f'{datetime.now()}: {message}')


def check_giveaway(headers, channel):
    try:
        channelID = channel['id']
        messages = requests.get(
            f'https://discord.com/api/v9/channels/{channelID}/messages?limit=50',
            headers=headers,
        ).json()

        if 'message' in messages:
            if messages['message'] == '401: Unauthorized':
                printt(f'Token expired: {headers["Authorization"][:9]}...')
            else:
                printt(f'Error in {channel["name"]}: {messages["message"]}')
            return

        for message in messages:
            for mention in message.get('mentions', []):
                if DC_NAME in mention['username']:
                    print(f"** {config.emoji} You win the giveaway in {channel['name']}")
    except Exception as e:
        printt(f'Error in {channel["name"]}: {e}')
        print(traceback.format_exc())


def main():
    try:
        for bot in config.bots:
            headers = config.headers
            headers['Authorization'] = bot['token']
            channels = bot['channels'].copy()
            for c in channels:
                check_giveaway(headers, c)
    except KeyboardInterrupt:
        sys.exit()


if __name__ == '__main__':
    main()
