import time
from http.client import HTTPSConnection
from sys import stderr
from json import dumps, loads
from time import sleep
from runBetData import RunBetData
import random


op_data = RunBetData()
header_data = {
    "content-type": "application/json",
    "user-agent": "discordapp.com",
    "authorization": op_data.get_discord_token(),
    "host": "discordapp.com",
    "referer": op_data.get_discord_link()
}


def get_connection():
    return HTTPSConnection("discordapp.com", 443)


def send_message(conn, channel_id, message_data, message_title):
    try:
        conn.request("POST", f"/api/v6/channels/{channel_id}/messages", message_data, header_data)
        resp = conn.getresponse()
        if 199 < resp.status < 300:
            print("> Send success: " + str(message_title))
        else:
            stderr.write(f"HTTP recibido {resp.status}: {resp.reason}\n")
    except Exception as ex:
        print(str(ex))


def get_random_message(conn, channel_id, limit=50):
    try:
        path = "/api/v6/channels/{}/messages?limit={}".format(channel_id, limit)
        conn.request("GET", path, None, header_data)
        resp = conn.getresponse()

        if 199 < resp.status < 300:
            selected_str = ''
            body = loads(resp.read())
            while len(selected_str) <= 3:
                pick_id = int(random.random() * limit)
                selected_str = body[pick_id]['content']
            print("> Pick message: {}".format(selected_str))
            return selected_str
            pass
        else:
            stderr.write(f"HTTP recibido {resp.status}: {resp.reason}\n")
            pass
    except Exception as ex:
        print(str(ex))




def main():
    while True:
        try:
            selected_msg = get_random_message(get_connection(), op_data.get_discord_channel_id())
            reply_msg = op_data.get_robot_reply(selected_msg);
            message_data = {
                "content": reply_msg,
                "tts": "false",
            }
            send_message(get_connection(), op_data.get_discord_channel_id(), dumps(message_data), reply_msg)
            time.sleep(op_data.get_discord_time_interval())
        except Exception as ex:
            import traceback
            traceback.print_exc()
            print(str(ex))




if __name__ == '__main__':
    while True:
        main()
        sleep(3600)  # every 1 hour = 3600