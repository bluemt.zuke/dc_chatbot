# coding=utf-8
import requests
from getpass import getpass

"""
簡化拿取token的方法，會執行登入去取得新的token，每次執行都會產生新的token。
不要重複執行，可能會被要求驗證，等到token再執行一次即可。
無法跑在Google colab上，Discord登入時會檔colab IP。

執行方式: python login_token.py
"""


def create_auth(username, password):
    url = 'https://discord.com/api/v9/auth/login'
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:97.0) Gecko/20100101 Firefox/97.0',
        'Accept': '*/*',
        'Content-Type': 'application/json',
        'Origin': 'https://discord.com',
        'Referer': 'https://discord.com/login'}
    req_json = {"login": username, "password": password,
                "undelete": False, "captcha_key": None,
                "login_source": None, "gift_code_sku_id": None}
    resp = requests.post(url, json=req_json, headers=headers)
    if resp.status_code == 200:
        return resp.json()['token']
    else:
        print("Login error:")
        print(resp.content)
        return None;


if __name__ == '__main__':
    u = input("Discord 登入帳號(Email): ")
    p = getpass("Discord 登入密碼: ")
    token = create_auth(u, p)
    print("")
    if token:
        print("Your token: %s" % token)
    else:
        print("Login failed. Please confirm your username / password again.")

